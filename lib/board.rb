class Board
  attr_reader :grid
  
  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
      @grid = grid
  end

  def display
    grid.each_with_index { |row, i| print "#{i}: #{row}\n" }
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    if pos
      self[pos].nil?
    else
      count == 0 if pos.nil?
    end
  end

  def full?
    count == grid.flatten.length
  end

  def won?
    count == 0
  end

  def place_random_ship
    raise "Board is full" if full?
    x = rand(0...grid.length)
    y = rand(0...grid.first.length)
    self[[x, y]] = :s
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end
end

if __FILE__ == $PROGRAM_NAME
  b = Board.new
  b.display
end
